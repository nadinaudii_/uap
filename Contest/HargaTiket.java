package Contest;

// Interface HargaTiket digunakan untuk menyediakan kontrak terkait harga tiket
interface HargaTiket {
    // Do your magic here...
    // Method akan mengembalikan harga tiket dalam bentuk integer
    int getTiketPrice();
}