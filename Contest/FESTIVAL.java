package Contest;

//subclass dari TiketKonser
class FESTIVAL extends TiketKonser {
    // Do your magic here.
    //terdapat constructor untuk menginisialisasi objek CAT1 dengan nilai dari tiketName dan tiketPrice yang diwarisi dari superclass 'TiketKOnser'
    public FESTIVAL() {
        super("FESTIVAL", 500000);
    }
}