package Contest;

// Exception class untuk menangani input yang tidak valid
public class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        // Memanggil constructor dari superclass (Exception) dengan pesan kesalahan yang diberikan
        super(message);
    }
}
