package Contest;

//subclass dari TiketKonser
class CAT1 extends TiketKonser {
    // Do your magic here...
    //terdapat constructor untuk menginisialisasi objek CAT1 dengan nilai dari tiketName dan tiketPrice yang diwarisi dari superclass 'TiketKOnser'
    public CAT1() {
        super("CAT1", 4000000);
    }
}