package Contest;

import java.util.Date;
import java.text.SimpleDateFormat;

class PemesananTiket {
    // Do your magic here...
    private String namaPemesan;
    private String kodePesanan;
    private Date tanggalPesanan;
    private String namaTiket;
    private int totalHarga;

    //constructor untuk menginisialisasi objek PemesananTiket
    public PemesananTiket(String namaPemesan, String namaTiket, int totalHarga) {
        this.namaPemesan = namaPemesan;
        this.namaTiket = namaTiket;
        this.totalHarga = totalHarga;
        this.kodePesanan = generateKodeBooking();
        this.tanggalPesanan = new Date();
    }

    //Getter untuk mendapatkan nama pemesanan
    public String getNamaPemesan() {
        return namaPemesan;
    }

    //Getter untuk mendapatkan nama kode pesanan
    public String getKodePesanan() {
        return kodePesanan;
    }

    //Getter untuk mendapatkan tanggal pesanan
    public Date getTanggalPesanan() {
        return tanggalPesanan;
    }

    //Getter untuk mendapatkan nama tiket
    public String getNamaTiket() {
        return namaTiket;
    }

    //Getter untuk mendapatkan total harga
    public int getTotalHarga() {
        return totalHarga;
    }

    //method untuk mencetak detail pesanan
    public void printDetailPesanan() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = dateFormat.format(tanggalPesanan);

        System.out.println("\nDetail Pesanan:");
        System.out.println("Nama Pemesan: " + namaPemesan);
        System.out.println("Kode Pesanan: " + kodePesanan);
        System.out.println("Tanggal Pesanan: " + formattedDate);
        System.out.println("Tiket yang dipesan: " + namaTiket);
        System.out.println("Total Harga: " + totalHarga);
    }

    //Method untuk menghasilkan kode pesanan secara acak
    private String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

}