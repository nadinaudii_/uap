/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;
import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        // Do your magic here...
        Scanner input = new Scanner(System.in);

        //try digunakan untuk menangkap dan menangani pengecualian yg terjadi saat pengguna menginput
        try {
            System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = input.nextLine();
            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT8");
            System.out.println("2. CAT1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. VVIP (UNLIMITED EXPERIENCE)");
            System.out.print("Masukkan pilihan: ");

            //if untuk memeriksa apakah input user bertipe int atau tidak
            //jika tidak, maka akan mengeluarkan error message
            if (!input.hasNextInt()) {
                throw new InvalidInputException(
                        "Nomor tiket tidak valid. Masukkan nomor tiket dalam bentuk angka.");
            }

            //untuk memeriksa apakah yang diinput user sesuai dengan menu yg ada (1-5).
            //jika tidak sesuai, maka akan mengeluarkan error message
            int nomorTiket = input.nextInt();
            if (nomorTiket < 1 || nomorTiket > 5) {
                throw new InvalidInputException(
                        "Nomor tiket tidak valid. Masukkan nomor tiket antara 1 hingga 5.");
            }

            //Sesuai dengan nomor tiket yang diinput, objek tiket konser yang sesuai akan dibuat menggunakan operator new dan ditugaskan ke variabel ticket yang bertipe TiketKonser.
            //Misalnya, jika pengguna memilih nomor tiket 1, maka objek CAT8 akan dibuat dan ditugaskan ke variabel ticket.
            TiketKonser ticket;
            switch (nomorTiket) {
                case 1:
                    ticket = new CAT8();
                    break;
                case 2:
                    ticket = new CAT1();
                    break;
                case 3:
                    ticket = new FESTIVAL();
                    break;
                case 4:
                    ticket = new VIP();
                    break;
                case 5:
                    ticket = new VVIP();
                    break;
                default:
                    throw new InvalidInputException("Terjadi kesalahan dalam memilih tiket.");
            }

            String kodePesanan = generateKodeBooking();
            //digunakan untuk mengembalikan sebuah kode pesanan secara acak terdiri dari 8 karakter dan isimpan dalam kedePesanan

            String tanggalPesanan = getCurrentDate();
            //digunakan memanggil method dan mengembalikan tanggal dalam format "dd-mm-yyyy" dan disimpan dalam tanggalPesanan
            System.out.println("------------ Detail Pesanan -----------");
            System.out.println("Nama Pemesan: " + namaPemesan);
            System.out.println("Kode Booking: " + kodePesanan);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + ticket.getTiketName());
            System.out.println("Total Harga: " + ticket.getTiketPrice());

        //menampilkan pesan kesalahan dari pengecualian
        } catch (InvalidInputException e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan yang tidak diketahui: " + e.getMessage());
        }

    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}