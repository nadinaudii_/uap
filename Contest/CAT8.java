package Contest;

//subclass dari TiketKonser
class CAT8 extends TiketKonser {
    // Do your magic here...
    //terdapat constructor untuk menginisialisasi objek CAT8 dengan nilai dari tiketName dan tiketPrice yang diwarisi dari superclass 'TiketKOnser'
    public CAT8() {
        super("CAT8", 800000);
    }
}