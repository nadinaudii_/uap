package Contest;

//subclass dari TiketKonser
class VVIP extends TiketKonser {
    // Do your magic here...
    public VVIP() {
        // Memanggil konstruktor superclass TiketKonser dengan argumen "VVIP (UNLIMITED EXPERIENCE)" dan 8000000
        super("VVIP (UNLIMITED EXPERIENCE)", 8000000);
    }
}