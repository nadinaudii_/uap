package Contest;

//subclass dari TiketKonser
class VIP extends TiketKonser {
    // Do your magic here...
    // Memanggil constructor superclass (TiketKonser) dengan nilai "VIP" dan 6000000
    public VIP() {
        super("VIP", 6000000);
    }
}