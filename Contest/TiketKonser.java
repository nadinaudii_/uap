package Contest;

// Abstraksi class TiketKonser
abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    private String tiketName;
    private int tiketPrice;

    // Constructor TiketKonser
    public TiketKonser(String tiketName, int tiketPrice) {
        this.tiketName = tiketName;
        this.tiketPrice = tiketPrice;
    }

    // Getter untuk mendapatkan nama tiket
    public String getTiketName() {
        return tiketName;
    }

    // Getter untuk mendapatkan harga tiket
    public int getTiketPrice() {
        return tiketPrice;
    }
}